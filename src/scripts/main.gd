#Copyright (C) 2020  Helena Aires

extends Node2D

signal level_finished

var chests_opened = []

var current_scene = Levels.get_scene(Enums.Levels.CHILDHOOD,Enums.Scenes.PASTEL).instance()

func _ready():
	connect("level_finished",self,"on_level_finished")
	add_child(current_scene)
	for i in range(0,current_scene.get_node("outline/YSort/chests").get_child_count()):
		chests_opened.append(false)
	Global.set_memory_count(chests_opened.size())
	$UI/base_ui.start()

func change_scene(scene_color):
	Global.set_scene(scene_color)
	var new_scene = Levels.get_current_scene().instance()
	
	var ghost_position = current_scene.get_node("outline/YSort/Ghost").position
	new_scene.get_node("outline/YSort/Ghost").position = ghost_position
	
	call_deferred("add_child",new_scene)
	call_deferred("remove_child",current_scene)
	current_scene = new_scene
	
	call_deferred("_update_chests")
	$UI/base_ui.update()
	get_tree().paused = false

func choose_scene():
	get_tree().paused = true
	$UI/Choice.popup_centered()

func open_chest(index):
	chests_opened[index] = true
	var finished = Global.add_memory()
	$UI/base_ui.update_memory()
	if finished:
		emit_signal("level_finished")

func on_level_finished():
	$UI/Finished.popup_centered()
	get_tree().paused = true
	Global.save()

func on_next_level():
	get_tree().quit()

func _update_chests():
	var chests = current_scene.get_node("outline/YSort/chests").get_children()
	for i in range(0,chests.size()):
		chests[i].set_open(chests_opened[i])

func on_menu_clicked(option):
	match option:
		Enums.Menu.PAUSE:
			if get_tree().is_paused():
				get_tree().paused = false
			else:
				get_tree().paused = true
		Enums.Menu.LOG:
			pass
		Enums.Menu.CONFIG:
			pass
		Enums.Menu.QUIT:
			get_tree().quit()
