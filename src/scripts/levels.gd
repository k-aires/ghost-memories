#Copyright (C) 2020  Helena Aires

extends Node

var levels = {
	Enums.Levels.CHILDHOOD: {
		Enums.Scenes.PASTEL: preload("res://levels/childhood/pastel.tscn"),
		Enums.Scenes.BLUE: preload("res://levels/childhood/blue.tscn"),
		Enums.Scenes.PINK: preload("res://levels/childhood/pink.tscn")
	},
	Enums.Levels.ADOLESCENCE: {
		Enums.Scenes.PASTEL: null,
		Enums.Scenes.BLUE: null,
		Enums.Scenes.PINK: null
	}
}

func get_current_scene():
	var level = Global.get_level()
	var scene = Global.get_scene()
	return levels[level][scene]

func get_scene(level,scene):
	return levels[level][scene]
