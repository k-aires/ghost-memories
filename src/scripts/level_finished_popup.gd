#Copyright (C) 2020  Helena Aires

extends Popup

signal next_level

func _ready():
	connect("next_level",get_tree().current_scene,"on_next_level")

func about_to_show():
	var current_color = Global.get_current_color()
	$VBoxContainer/Label.set("custom_colors/font_color",current_color)
	$VBoxContainer/Button.set("custom_colors/font_color_disabled",current_color)
	$VBoxContainer/Button.set("custom_colors/font_color",current_color)
	$VBoxContainer/Button.set("custom_colors/font_color_hover",current_color)
	$VBoxContainer/Button.set("custom_colors/font_color_pressed",current_color)

func _on_button_pressed():
	emit_signal("next_level")
