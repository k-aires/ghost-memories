#Copyright (C) 2020  Helena Aires

extends Node

enum Levels {
	CHILDHOOD,
	ADOLESCENCE
}

enum Scenes {
	PASTEL,
	BLUE,
	PINK
}

enum Pathways {
	PASTEL_TO_BLUE,
	PASTEL_TO_PINK,
	BLUE_TO_PINK,
	CHOICE
}

enum Menu {
	PAUSE,
	LOG,
	CONFIG,
	QUIT
}
