#Copyright (C) 2020  Helena Aires

extends KinematicBody2D

const ACCELERATION = 80
const MAX_SPEED = 75
const FRICTION = 80

var velocity = Vector2.ZERO
var target = Vector2.ZERO

onready var animation_tree = $AnimationTree
onready var animation_state = $AnimationTree.get("parameters/playback")
onready var interaction = $Area2D

func _ready():
	$RemoteTransform2D.set_remote_node(get_tree().current_scene.get_node("Camera2D").get_path())
	animation_tree.active = true

func _physics_process(delta):
	_move(delta)
	_interact()

func _move(delta):
	var input = Vector2.ZERO
	input.x = Input.get_action_strength("ui_right")-Input.get_action_strength("ui_left")
	input.y = Input.get_action_strength("ui_down")-Input.get_action_strength("ui_up")
	input = input.normalized()
	
	if input != Vector2.ZERO:
		animation_tree.set("parameters/idle/blend_position", input)
		animation_tree.set("parameters/move/blend_position", input)
		animation_state.travel("move")
		velocity = velocity.move_toward(input*MAX_SPEED,ACCELERATION*delta)
	else:
		animation_state.travel("idle")
		velocity = velocity.move_toward(Vector2.ZERO,FRICTION*delta)
	
	velocity = move_and_slide(velocity)

func _interact():
	if Input.is_action_just_pressed("ui_select"):
		$interaction_player.play("interact")
