#Copyright (C) 2020  Helena Aires

extends StaticBody2D

signal interacted_with_mirror
signal choice_mirror

export(preload("res://scripts/enums.gd").Pathways) var pathway = Enums.Pathways.PASTEL_TO_BLUE

onready var sprite = $Sprite

func _ready():
	connect("interacted_with_mirror",get_tree().current_scene,"change_scene")
	connect("choice_mirror",get_tree().current_scene,"choose_scene")

func update():
	var active = true
	var frame_coords = Vector2.ZERO
	
	match Global.get_scene():
		Enums.Scenes.PASTEL:
			if pathway == Enums.Pathways.BLUE_TO_PINK:
				active = false
		Enums.Scenes.BLUE:
			if pathway == Enums.Pathways.PASTEL_TO_PINK:
				active = false
		Enums.Scenes.PINK:
			if pathway == Enums.Pathways.PASTEL_TO_BLUE:
				active = false
	
	frame_coords.x = Global.get_scene()
	if !active:
		frame_coords.y = 1
	sprite.set_frame_coords(frame_coords)

func _on_interaction(area):
	match pathway:
		Enums.Pathways.PASTEL_TO_BLUE:
			if Global.get_scene() == Enums.Scenes.PASTEL:
				emit_signal("interacted_with_mirror",Enums.Scenes.BLUE)
			elif Global.get_scene() == Enums.Scenes.BLUE:
				emit_signal("interacted_with_mirror",Enums.Scenes.PASTEL)
		Enums.Pathways.PASTEL_TO_PINK:
			if Global.get_scene() == Enums.Scenes.PASTEL:
				emit_signal("interacted_with_mirror",Enums.Scenes.PINK)
			elif Global.get_scene() == Enums.Scenes.PINK:
				emit_signal("interacted_with_mirror",Enums.Scenes.PASTEL)
		Enums.Pathways.BLUE_TO_PINK:
			if Global.get_scene() == Enums.Scenes.BLUE:
				emit_signal("interacted_with_mirror",Enums.Scenes.PINK)
			elif Global.get_scene() == Enums.Scenes.PINK:
				emit_signal("interacted_with_mirror",Enums.Scenes.BLUE)
		Enums.Pathways.CHOICE:
			emit_signal("choice_mirror")
