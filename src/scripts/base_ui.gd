extends Control

var memory_gem = preload("res://ui/memory_gem.tscn")

func start():
	for i in range(0, Global.get_memory_count()):
		$gems.add_child(memory_gem.instance())
	update()

func update_memory():
	var memory = $gems.get_child(Global.get_current_memories()-1).get_node("Sprite")
	var frame_coords = memory.get_frame_coords()
	frame_coords.y = 1
	memory.set_frame_coords(frame_coords)

func update():
	var frame_coords = Vector2.ZERO
	for gem in $gems.get_children():
		frame_coords = gem.get_node("Sprite").get_frame_coords()
		frame_coords.x = Global.get_scene()
		gem.get_node("Sprite").set_frame_coords(frame_coords)
	
	$Label.set("custom_colors/font_color", Global.get_current_color())
#	$Label.set("custom_colors/font_outline_modulate", Global.get_current_color())
#	$Label.set("custom_colors/font_color_shadow", Global.get_current_color())
	
	for option in $Menu.get_children():
		option.update()
