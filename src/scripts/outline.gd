#Copyright (C) 2020  Helena Aires

extends Node2D

export(preload("res://scripts/enums.gd").Scenes) var scene = Enums.Scenes.PASTEL

onready var background = $Background
onready var mirrors = $YSort/mirrors
onready var chests = $YSort/chests

func _ready():
	background.set_frame_color(Global.get_current_color())
	update()

func update():
	for mirror in mirrors.get_children():
		mirror.update()
	for chest in chests.get_children():
		chest.update()
